#!/usr/bin/gnuplot -persist

set datafile separator ';'
set timefmt "%d/%m/%Y-%H.%M"
set xdata time
set format x "%d/%m/%Y\n%H.%M"
set xtics out nomirror rotate by -45

set multiplot layout 2,1

set title "User 1"
plot \
  'data.csv' i 0 using (stringcolumn(1).'-'.stringcolumn(2)):3 every ::1 with lp title "systolic", \
  'data.csv' i 0 using (stringcolumn(1).'-'.stringcolumn(2)):4 every ::1 with lp title "diastolic", \
  'data.csv' i 0 using (stringcolumn(1).'-'.stringcolumn(2)):5 every ::1 with lp title "pulses"

set title "User 2"
plot \
  'data.csv' i 1 using (stringcolumn(1).'-'.stringcolumn(2)):3 every ::1 with lp title "systolic", \
  'data.csv' i 1 using (stringcolumn(1).'-'.stringcolumn(2)):4 every ::1 with lp title "diastolic", \
  'data.csv' i 1 using (stringcolumn(1).'-'.stringcolumn(2)):5 every ::1 with lp title "pulses"

unset multiplot
